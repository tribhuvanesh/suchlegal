name := "SuchLegal"

organization := "ch.ethz.dalab"

version := "1.0"

scalaVersion := "2.10.4"

libraryDependencies += "ch.ethz.dalab" %% "dissolvestruct" % "0.1-SNAPSHOT"

libraryDependencies += "org.apache.spark" %% "spark-core" % "1.2.1"

libraryDependencies += "org.apache.spark" %% "spark-mllib" % "1.2.1"



    