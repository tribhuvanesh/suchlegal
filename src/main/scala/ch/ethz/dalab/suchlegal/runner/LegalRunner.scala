package ch.ethz.dalab.suchlegal.runner

import breeze.linalg.Vector
import ch.ethz.dalab.dissolve.classification.BinarySVMWithDBCFW
import ch.ethz.dalab.dissolve.regression.LabeledObject
import ch.ethz.dalab.dissolve.utils.cli.CLAParser
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.util.MLUtils
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
 * Created by tribhu on 18/03/15.
 */
object LegalRunner {

  def main(args: Array[String]): Unit = {
    val (solverOptions, kwargs) = CLAParser.argsToOptions[Vector[Double], Double](args)
    val dataPath = kwargs.getOrElse("input_path", "/user/roskarr/docvec_libsvm")
    val appname = kwargs.getOrElse("appname", "such_legal")
    val debugPath = kwargs.getOrElse("debug_file", "such_legal-%d.csv".format(System.currentTimeMillis() / 1000))
    val dataSample = kwargs.getOrElse("perc_train", "0.1").toDouble
    solverOptions.debugInfoPath = debugPath

    println(dataPath)
    println(kwargs)

    // Fix seed for reproducibility
    util.Random.setSeed(1)

    val conf = new SparkConf().setAppName(appname)
    val sc = new SparkContext(conf)
    sc.setCheckpointDir("checkpoint-files")

    val data = MLUtils
      .loadLibSVMFile(sc, dataPath)
      .sample(withReplacement = false, dataSample, 42)
      .map {
      // dissolve^struct expects labels to be in +1/-1 format
      case x: LabeledPoint =>
        val label =
          if (x.label > 0.5)
            +1.00
          else
            -1.00
        LabeledPoint(label, x.features)
    }

    // Split data into training and test set
    val splits = data.randomSplit(Array(0.8, 0.2), seed = 1L)
    val trainingRDD = splits(0)
    val testRDD = splits(1)

    val testRDDLo: RDD[LabeledObject[Vector[Double], Double]] =
      testRDD.map {
        case x: LabeledPoint =>
          new LabeledObject[Vector[Double], Double](x.label, Vector(x.features.toArray)) // Is the asInstanceOf required?
      }

    solverOptions.testDataRDD = Some(testRDDLo)
    val model = BinarySVMWithDBCFW.train(trainingRDD, solverOptions)

    // Test Errors
    val trueTestPredictions =
      testRDDLo.map {
        case x: LabeledObject[Vector[Double], Double] =>
          val prediction = model.predict(x.pattern)
          if (prediction == x.label)
            1
          else
            0
      }.fold(0)((acc, ele) => acc + ele)

    println("Accuracy on Test set = %d/%d = %.4f".format(trueTestPredictions,
      testRDDLo.count(),
      (trueTestPredictions.toDouble / testRDDLo.count().toDouble) * 100))

  }

}
